#include <iostream>
#ifdef _MACOSX
#include <OpenCL/opencl.h>
#else
#include <CL/opencl.h>
#endif

#define MAX_ITEMS 32

#define CHECK_CL(call) do{\
	cl_uint err = call;\
	if (err != CL_SUCCESS) {\
		cerr << "OpenCL error: '" << opencl_error(err) << "' at " << __FILE__ << ":" << __LINE__ << endl; \
		exit(1);\
	}\
}while(0)

using namespace std;

const char* opencl_error(cl_uint err)
{
    switch (err) {
        case CL_SUCCESS:                            return "Success!";
        case CL_DEVICE_NOT_FOUND:                   return "Device not found";
        case CL_DEVICE_NOT_AVAILABLE:               return "Device not available";
        case CL_COMPILER_NOT_AVAILABLE:             return "Compiler not available";
        case CL_MEM_OBJECT_ALLOCATION_FAILURE:      return "Memory object allocation failure";
        case CL_OUT_OF_RESOURCES:                   return "Out of resources";
        case CL_OUT_OF_HOST_MEMORY:                 return "Out of host memory";
        case CL_PROFILING_INFO_NOT_AVAILABLE:       return "Profiling information not available";
        case CL_MEM_COPY_OVERLAP:                   return "Memory copy overlap";
        case CL_IMAGE_FORMAT_MISMATCH:              return "Image format mismatch";
        case CL_IMAGE_FORMAT_NOT_SUPPORTED:         return "Image format not supported";
        case CL_BUILD_PROGRAM_FAILURE:              return "Program build failure";
        case CL_MAP_FAILURE:                        return "Map failure";
        case CL_INVALID_VALUE:                      return "Invalid value";
        case CL_INVALID_DEVICE_TYPE:                return "Invalid device type";
        case CL_INVALID_PLATFORM:                   return "Invalid platform";
        case CL_INVALID_DEVICE:                     return "Invalid device";
        case CL_INVALID_CONTEXT:                    return "Invalid context";
        case CL_INVALID_QUEUE_PROPERTIES:           return "Invalid queue properties";
        case CL_INVALID_COMMAND_QUEUE:              return "Invalid command queue";
        case CL_INVALID_HOST_PTR:                   return "Invalid host pointer";
        case CL_INVALID_MEM_OBJECT:                 return "Invalid memory object";
        case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR:    return "Invalid image format descriptor";
        case CL_INVALID_IMAGE_SIZE:                 return "Invalid image size";
        case CL_INVALID_SAMPLER:                    return "Invalid sampler";
        case CL_INVALID_BINARY:                     return "Invalid binary";
        case CL_INVALID_BUILD_OPTIONS:              return "Invalid build options";
        case CL_INVALID_PROGRAM:                    return "Invalid program";
        case CL_INVALID_PROGRAM_EXECUTABLE:         return "Invalid program executable";
        case CL_INVALID_KERNEL_NAME:                return "Invalid kernel name";
        case CL_INVALID_KERNEL_DEFINITION:          return "Invalid kernel definition";
        case CL_INVALID_KERNEL:                     return "Invalid kernel";
        case CL_INVALID_ARG_INDEX:                  return "Invalid argument index";
        case CL_INVALID_ARG_VALUE:                  return "Invalid argument value";
        case CL_INVALID_ARG_SIZE:                   return "Invalid argument size";
        case CL_INVALID_KERNEL_ARGS:                return "Invalid kernel arguments";
        case CL_INVALID_WORK_DIMENSION:             return "Invalid work dimension";
        case CL_INVALID_WORK_GROUP_SIZE:            return "Invalid work group size";
        case CL_INVALID_WORK_ITEM_SIZE:             return "Invalid work item size";
        case CL_INVALID_GLOBAL_OFFSET:              return "Invalid global offset";
        case CL_INVALID_EVENT_WAIT_LIST:            return "Invalid event wait list";
        case CL_INVALID_EVENT:                      return "Invalid event";
        case CL_INVALID_OPERATION:                  return "Invalid operation";
        case CL_INVALID_GL_OBJECT:                  return "Invalid OpenGL object";
        case CL_INVALID_BUFFER_SIZE:                return "Invalid buffer size";
        case CL_INVALID_MIP_LEVEL:                  return "Invalid mip-map level";
        default: return "Unknown";
    }
}

int main(int argc, char** argv)
{
	cl_uint num_platforms;
	cl_platform_id platforms[MAX_ITEMS];
	CHECK_CL(clGetPlatformIDs(MAX_ITEMS, platforms, &num_platforms));

	for (cl_uint i=0; i<num_platforms; i++)
	{
		cout << "* Platform #" << i << "\n";

		char buffer[64];

		CHECK_CL(clGetPlatformInfo(platforms[i],
			CL_PLATFORM_VERSION, sizeof(buffer), buffer, NULL));
		cout << "| Version    : " << buffer << "\n";

		CHECK_CL(clGetPlatformInfo(platforms[i],
			CL_PLATFORM_NAME, sizeof(buffer), buffer, NULL));
		cout << "| Name       : " << buffer << "\n";

		CHECK_CL(clGetPlatformInfo(platforms[i],
			CL_PLATFORM_VENDOR, sizeof(buffer), buffer, NULL));
		cout << "| Vendor     : " << buffer << "\n";

		//CHECK_CL(clGetPlatformInfo(platforms[i], CL_PLATFORM_EXTENSIONS, sizeof(buffer), buffer, NULL));
		//cout << "| Extensions : " << buffer << "\n";

		cl_uint num_devices;
		cl_device_id devices[MAX_ITEMS];
		CHECK_CL(clGetDeviceIDs(platforms[i],
			CL_DEVICE_TYPE_ALL, MAX_ITEMS, devices, &num_devices));

		char sep = '|';

		for (cl_uint j=0; j<num_devices; j++)
		{
			cl_bool b;
			cl_device_type t;
			cl_ulong ul;
			cl_uint ui;
			size_t s[3];

			if (j < (num_devices - 1)) {
				cout << "|-* Device #" << j << "\n";
			} else {
				sep = ' ';
				cout << "--* Device #" << j << "\n";
			}

			CHECK_CL(clGetDeviceInfo(devices[j],
				CL_DEVICE_NAME, sizeof(buffer), buffer, NULL));
			cout << sep << " | Name          : " << buffer << "\n";

			CHECK_CL(clGetDeviceInfo(devices[j],
				CL_DEVICE_TYPE, sizeof(t), &t, NULL));
			cout << sep << " | Type          : ";
			switch (t) {
				case CL_DEVICE_TYPE_CPU: cout << "CPU\n"; break;
				case CL_DEVICE_TYPE_GPU: cout << "GPU\n"; break;
				case CL_DEVICE_TYPE_ACCELERATOR: cout << "Accelerator\n"; break;
				case CL_DEVICE_TYPE_DEFAULT: cout << "Default\n"; break;
				default: cout << "Unknown\n";
			}

			CHECK_CL(clGetDeviceInfo(devices[j],
				CL_DEVICE_MAX_COMPUTE_UNITS, sizeof(ui), &ui, NULL));
			cout << sep << " | Compute Cores : " << ui << "\n";

			CHECK_CL(clGetDeviceInfo(devices[j],
				CL_DEVICE_MAX_CLOCK_FREQUENCY, sizeof(ui), &ui, NULL));
			cout << sep << " | Core Freq.    : " << ui << " Mhz\n";

			CHECK_CL(clGetDeviceInfo(devices[j],
				CL_DEVICE_VENDOR, sizeof(buffer), buffer, NULL));
			cout << sep << " | Vendor        : " << buffer << "\n";

			CHECK_CL(clGetDeviceInfo(devices[j],
				CL_DEVICE_VENDOR_ID, sizeof(ui), &ui, NULL));
			cout << sep << " | Vendor ID     : " << ui << "\n";

			CHECK_CL(clGetDeviceInfo(devices[j],
				CL_DEVICE_VERSION, sizeof(buffer), buffer, NULL));
			cout << sep << " | Version       : " << buffer << "\n";

			CHECK_CL(clGetDeviceInfo(devices[j],
				CL_DRIVER_VERSION, sizeof(buffer), buffer, NULL));
			cout << sep << " | Driver Ver.   : " << buffer << "\n";

			CHECK_CL(clGetDeviceInfo(devices[j],
				CL_DEVICE_AVAILABLE, sizeof(b), &b, NULL));
			cout << sep << " | Available     : ";
			switch (b) {
				case CL_TRUE: cout << "Yes\n"; break;
				case CL_FALSE: cout << "No\n"; break;
				default: cout << "Unknown\n";
			}

			CHECK_CL(clGetDeviceInfo(devices[j],
				CL_DEVICE_MAX_WORK_ITEM_SIZES, sizeof(s), s, NULL));
			cout << sep << " | Max Items     : ("
			     << s[0] << ',' << s[1] << ',' << s[2] << ")\n";

			CHECK_CL(clGetDeviceInfo(devices[j],
				CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), s, NULL));
			cout << sep << " | Max Groups    : " << s[0] << "\n";

			CHECK_CL(clGetDeviceInfo(devices[j],
				CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE, sizeof(ul), &ul, NULL));
			cout << sep << " | Max Constant  : " << ul << " kB\n";

			CHECK_CL(clGetDeviceInfo(devices[j],
				CL_DEVICE_MAX_CONSTANT_ARGS, sizeof(ui), &ui, NULL));
			cout << sep << " | Max Constants : " << ui << "\n";

			CHECK_CL(clGetDeviceInfo(devices[j],
				CL_DEVICE_LOCAL_MEM_SIZE, sizeof(ul), &ul, NULL));
			cout << sep << " | Local Mem.    : " << (ul/1024) << " kB\n";

			CHECK_CL(clGetDeviceInfo(devices[j],
				CL_DEVICE_GLOBAL_MEM_SIZE, sizeof(ul), &ul, NULL));
			cout << sep << " | Global Mem.   : " << (ul/1024) << " kB\n";

			CHECK_CL(clGetDeviceInfo(devices[j],
				CL_DEVICE_GLOBAL_MEM_CACHE_SIZE, sizeof(ul), &ul, NULL));
			cout << sep << " - Global Cache  : " << ul << " B\n";

		}
	}

	cout << num_platforms << " OpenCL platform(s) found." << endl;

	return 0;
}
