#!/bin/bash

IN=${1-"missing"}

for pkg in `cat $IN`; do
  R -e "install.packages('$pkg',repos='http://lib.stat.cmu.edu/R/CRAN')"
done
