// gcc -shared -o libusage.so usage.c -fPIC -lrt

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/time.h>
#include <sys/resource.h>

#define MAX_DEPTH 10

static char* prefix;
static long long init_nsec;
static int idepth;

long long get_current_nsec()
{
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	return 1000000000LL * (long long)ts.tv_sec + (long long)ts.tv_nsec;
}

double tv_to_sec(struct timeval * tv)
{
	unsigned long usec = tv->tv_usec + 1000000UL * tv->tv_sec;
	return (double)usec / 1000000.0;
}

__attribute__ ((constructor))
void init_usage()
{
	init_nsec = get_current_nsec();

	prefix = getenv("USAGE_PREFIX");
	if (prefix == NULL) {
		prefix = "usage:";
	}

	/* Don't propagate the preload of this library to children, unless
	   USAGE_CHILDREN is set in the caller's environment. */
	char* children = getenv("USAGE_CHILDREN");
	if (children == NULL) {
		unsetenv("LD_PRELOAD");
	} else {
		idepth = 0;
		int ichildren = atoi(children);
		char* depth = getenv("__USAGE_CHILDREN_CURRENT_DEPTH");
		if (depth) {
			idepth = atoi(depth);
			if (idepth >= ichildren || idepth >= MAX_DEPTH) {
				unsetenv("LD_PRELOAD");
			}
		}
		char newdepth[MAX_DEPTH];
		sprintf(newdepth, "%d", idepth + 1);
		setenv("__USAGE_CHILDREN_CURRENT_DEPTH", newdepth, 1);
	}
}

__attribute__ ((destructor))
void report_usage()
{
	/* Immediately stop the walltime clock, to avoid including the time
	   for this function. */
	long long end_nsec = get_current_nsec();

	/* Buffer the output, so that the entire line can be written to
	   stderr without a child process interrupting. */
	char outbuf[2048];
	char* out = outbuf;
	out += sprintf(out, "%s ", prefix);

	/* Report memory usage from /proc/self/status interface. */
	char line[80];

	FILE* status = fopen("/proc/self/status", "r");
	if (!status) {
		fprintf(stderr, "usage: unable to open /proc/self/status\n");
		perror("usage");
		return;
	}

	while (!feof(status)) {
		fgets(line, 80, status);
		if (strncmp(line, "Name:", 5) == 0) {
			char* c = strchr(line, '\n');
			*c = '\0';
			out += sprintf(out, "name=%s", line + 6);
		} else if (strncmp(line, "VmPeak:", 7) == 0) {
			long long unsigned kb = 0;
			sscanf(line, "VmPeak: ""%llu", &kb);
			out += sprintf(out, ",vmem=%lluk", kb);
		} else if (strncmp(line, "VmHWM:", 6) == 0) {
			long long unsigned kb = 0;
			sscanf(line, "VmHWM: ""%llu", &kb);
			out += sprintf(out, ",mem=%lluk", kb);
		}
	}

	fclose(status);

	/* Report elapsed walltime from clock_gettime() interface. */
	double walltime = (double)(end_nsec - init_nsec) / 1000000000.0;
	out += sprintf(out, ",walltime=%gs", walltime);

	/* Report sys vs. user time from rgetusage() interface. */
	struct rusage ru;
	getrusage(RUSAGE_SELF, &ru);
	double utime = tv_to_sec(&(ru.ru_utime));
	double stime = tv_to_sec(&(ru.ru_stime));
	getrusage(RUSAGE_CHILDREN, &ru);
	utime += tv_to_sec(&(ru.ru_utime));
	stime += tv_to_sec(&(ru.ru_stime));
	sprintf(out, ",usertime=%gs,systime=%gs,calldepth=%d\n", utime, stime, idepth);

	fprintf(stderr, outbuf);
}

