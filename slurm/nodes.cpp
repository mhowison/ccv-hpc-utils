#include <cstdlib>
#include <cstdio>
#include <cmath>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>

#include <slurm/slurm.h>

using namespace std;

#define PROGNAME "nodes"

#define STRY(statement) do{\
	int rc = statement ;\
	if (rc != SLURM_SUCCESS) { slurm_perror(PROGNAME); exit(SLURM_ERROR); }\
}while(0)

typedef vector<string> Column;

struct NodeGroup {
	string cpu;
	string mem;
	string features;
	unsigned nAlloc;
	unsigned nTotal;
	unsigned cAlloc;
	unsigned cTotal;
	NodeGroup() { nAlloc=nTotal=cAlloc=cTotal=0; }
};
typedef map<string,NodeGroup> Hash;

Hash online;
Hash offline;

Column nTotal(1, "NODES");
Column cTotal(1, "CORES");
Column cpu(1, "CPU");
Column mem(1, "MEM");
Column features(1, "FEATURES");
Column nAlloc(1, "NODES");
Column nAllocPer(1, "IN USE");
Column cAlloc(1, "CORES");
Column cAllocPer(1, "IN USE");
Column offlineTotal(1, "OFFLINE");
Column offlineFeatures(1, "");

void align_left(Column& column, const char* sep=" ")
{
	Column::iterator i;
	size_t m = 0;

	for (i=column.begin(); i!=column.end(); ++i) {
		m = max(m, i->size());
	}

	for (i=column.begin(); i!=column.end(); ++i) {
		int padding = m - i->size();
		if (padding > 0) i->append(padding, ' ');
		i->append(sep);
	}
}

void align_right(Column& column, const char* sep=" ")
{
	Column::iterator i;
	size_t m = 0;

	for (i=column.begin(); i!=column.end(); ++i) {
		size_t len = i->find_first_of('\n');
		if (len == string::npos) len = i->size();
		m = max(m, len);
	}

	for (i=column.begin(); i!=column.end(); ++i) {
		size_t len = i->find_first_of('\n');
		if (len == string::npos) len = i->size();
		int padding = m - len;
		if (padding > 0) i->insert(0, padding, ' ');
		i->append(sep);
	}
}

string int2str(unsigned a)
{
	stringstream sbuf;
	sbuf << a;
	return sbuf.str();
}

string percent(unsigned a, unsigned b)
{
	string s;
	char cbuf[6];
	sprintf(cbuf, "%.1f%%", (100.f * a) / b);
	s.assign(cbuf);
	return s;
}

int main(int argc, char** argv)
{
	node_info_msg_t* nodes;

	STRY( slurm_load_node(0, &nodes, SHOW_ALL) );
	
	NodeGroup all;

	for (uint32_t i=0; i<nodes->record_count; i++)
	{
		node_info_t* node = nodes->node_array + i;

		int16_t cores = node->cores * node->sockets;

		stringstream cpu;
		cpu << cores << "-core";

		stringstream mem;
		mem << (int)round(node->real_memory / 1024.f) << "GB";

		stringstream id;
		id << cpu.str() << mem.str() << node->features;

		int state = (node->node_state & NODE_STATE_BASE);
		if (state == NODE_STATE_ALLOCATED || state == NODE_STATE_MIXED || state == NODE_STATE_IDLE)
		{
			online[id.str()].cpu = cpu.str();
			online[id.str()].mem = mem.str();
			online[id.str()].features = node->features;

			online[id.str()].nTotal += 1;
			online[id.str()].cTotal += cores;

			all.nTotal += 1;
			all.cTotal += cores;

			if (state == NODE_STATE_ALLOCATED || state == NODE_STATE_MIXED) {
				online[id.str()].nAlloc += 1;
				online[id.str()].cAlloc += cores;
				all.nAlloc += 1;
				all.cAlloc += cores;
			}
		}
		else
		{
			offline[id.str()].nTotal += 1;
			offline[id.str()].cTotal += cores;
			offline[id.str()].cpu = cpu.str();
			offline[id.str()].mem = mem.str();
			offline[id.str()].features = node->features;
		}

	}

	for (Hash::iterator i = online.begin(); i != online.end(); ++i)
	{
		nTotal.push_back(int2str(i->second.nTotal));
		cTotal.push_back(int2str(i->second.cTotal));
		cpu.push_back(i->second.cpu);
		mem.push_back(i->second.mem);
		features.push_back(i->second.features);
		nAlloc.push_back(int2str(i->second.nAlloc));
		nAllocPer.push_back(percent(i->second.nAlloc, i->second.nTotal));
		cAlloc.push_back(int2str(i->second.cAlloc));
		cAllocPer.push_back(percent(i->second.cAlloc, i->second.cTotal));
	}

	/* add an extra line break before grand total */
	cAllocPer.back().push_back('\n');

	/* grand total */
	nTotal.push_back(int2str(all.nTotal));
	cTotal.push_back(int2str(all.cTotal));
	cpu.push_back("");
	mem.push_back("");
	features.push_back("");
	nAlloc.push_back(int2str(all.nAlloc));
	nAllocPer.push_back(percent(all.nAlloc, all.nTotal));
	cAlloc.push_back(int2str(all.cAlloc));
	cAllocPer.push_back(percent(all.cAlloc, all.cTotal));

	/* align columns */
	align_right(nTotal, "  ");
	align_right(cTotal, "  ");
	align_right(cpu, "  ");
	align_right(mem, "  ");
	align_left(features, "  ");
	align_right(nAlloc);
	align_right(nAllocPer, "   ");
	align_right(cAlloc);
	align_right(cAllocPer);

	/* print */
	for (unsigned i=0; i<nTotal.size(); i++) {
		cout << nTotal[i] << cTotal[i] << cpu[i] << mem[i] << features[i]
		     << nAlloc[i] << nAllocPer[i] << cAlloc[i] << cAllocPer[i] << '\n';
	}

	/* offline summary */
#if 0
	for (Hash::iterator i = offline.begin(); i != offline.end(); ++i)
	{
		offlineTotal.push_back(int2str(i->second.nTotal));
		offlineFeatures.push_back(i->first);
	}
	align_right(offlineTotal, " ");
	align_left(offlineFeatures);
	cout << '\n';
	for (unsigned i=0; i<nTotal.size(); i++) {
		cout << offlineTotal[i] << offlineFeatures[i] << '\n';
	}
#endif

	cout.flush();

	return EXIT_SUCCESS;
}

