#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <pwd.h>
#include <cerrno>
#include <unistd.h>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>

#include <slurm/slurm.h>

using namespace std;

#define PROGNAME "nodestat"

#define TRY(statement) do{\
	errno = 0;\
	statement ;\
	if (errno) { perror(PROGNAME); exit(EXIT_FAILURE); }\
}while(0)\

#define STRY(statement) do{\
	int rc = statement ;\
	if (rc != SLURM_SUCCESS) { slurm_perror(PROGNAME); exit(SLURM_ERROR); }\
}while(0)

typedef vector<string> Column;

Column host(1, "HOST");
Column cpus(1, "CORES");
Column mem(1, "MEM");
Column features(1, "FEATURES");
Column cpuload(1, "CPULOAD");
Column jobload(1, "!JOBS");

void align_left(Column& column, char sep=' ')
{
	Column::iterator i;
	size_t m = 0;

	for (i=column.begin(); i!=column.end(); ++i) {
		m = max(m, i->size());
	}

	for (i=column.begin(); i!=column.end(); ++i) {
		int padding = m - i->size();
		if (padding > 0) i->append(padding, ' ');
		i->push_back(sep);
	}
}

void align_right(Column& column, char sep=' ')
{
	Column::iterator i;
	size_t m = 0;

	for (i=column.begin(); i!=column.end(); ++i) {
		m = max(m, i->size());
	}

	for (i=column.begin(); i!=column.end(); ++i) {
		int padding = m - i->size();
		if (padding > 0) i->insert(0, padding, ' ');
		i->push_back(sep);
	}
}

int main(int argc, char** argv)
{
	node_info_msg_t* nodes;
	stringstream sbuf;

	STRY( slurm_load_node(0, &nodes, SHOW_ALL) );
	
	for (uint32_t i=0; i<nodes->record_count; i++)
	{
		node_info_t* node = nodes->node_array + i;

		sbuf.str(node->name);
		host.push_back(sbuf.str());

		sbuf.str("");
		sbuf << node->cores * node->sockets;
		cpus.push_back(sbuf.str());

		sbuf.str("");
		sbuf << (int)round(node->real_memory / 1024.f) << "GB";
		mem.push_back(sbuf.str());

		sbuf.str(node->features);
		features.push_back(sbuf.str());

		sbuf.str("");
		sbuf << node->cpu_load << '%';
		cpuload.push_back(sbuf.str());

		jobload.push_back("");
	}

	slurm_free_node_info_msg(nodes);

	job_info_msg_t* jobs;
	struct passwd pwd;
	struct passwd* pwdresult;
	size_t pwdbufsize;
	char* pwdbuf;
   
   	TRY( pwdbufsize = sysconf(_SC_GETPW_R_SIZE_MAX) );
	TRY( pwdbuf = (char*)malloc(pwdbufsize) );

	STRY( slurm_load_jobs(0, &jobs, SHOW_ALL) );

	for (uint32_t i=0; i<jobs->record_count; i++)
	{
		slurm_job_info_t* job = jobs->job_array + i;

		if (job->job_state == JOB_RUNNING) {

			TRY( getpwuid_r(job->user_id, &pwd, pwdbuf, pwdbufsize, &pwdresult) );
		
			sbuf.str("");
			sbuf << ',' << pwd.pw_name << ':' << job->job_id;

			for (unsigned j = 0; job->node_inx[j] != -1; j += 2)
				for (int k=job->node_inx[j]; k<=job->node_inx[j+1]; k++)
					jobload[k+1].append(sbuf.str());
		}
	}

	slurm_free_job_info_msg(jobs);

	align_left(host);
	align_right(cpus);
	align_right(mem);
	align_left(features);
	align_right(cpuload);

	for (unsigned i=0; i<host.size(); i++) {
		cout << host[i] << cpus[i] << mem[i] << features[i] << cpuload[i];
		if (jobload[i].size()) cout << (jobload[i].data() + 1) << "\n";
		else cout << "(idle)\n";
	}

	cout.flush();

	free(pwdbuf);

	return EXIT_SUCCESS;
}

