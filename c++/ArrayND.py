#!/usr/bin/env python
"""/*
 * ArrayND - a simple C++ class that emulates a Fortran array
 *
 * Copyright 2012, Brown University, Providence, RI. All Rights Reserved.
 *
 * This file is part of ccv-hpc-utils.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose other than its incorporation into a
 * commercial product is hereby granted without fee, provided that the
 * above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation, and that the name of Brown University not be used in
 * advertising or publicity pertaining to distribution of the software
 * without specific, written prior permission.
 *
 * BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
 * INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY
 * PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR
 * ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 * Author: Mark Howison <mhowison@brown.edu>
 */"""

import sys
from string import Template

if len(sys.argv) > 1:
	n = int(sys.argv[1])
	if n < 2:
		print "error: ArrayND only works for n >= 2"
		sys.exit(-1)
else:
	n = 2

name = 'Array%dD' % n
filename = name + '.hpp'

print "Generating %s ..." % filename

constructor_args = ', '.join('size_t dim%d' % (i+1) for i in range(n))
set_dims = '\n\t\t\t'.join('_dims[%d] = dim%d;' % (i,i+1) for i in range(n))
size_calc = ' * '.join('dim%d' % (i+1) for i in range(n))
index_args = ', '.join('size_t x%d' % (i+1) for i in range(n))
index_calc = ' + '.join('x%d*_subdims[%d]' % (i+1,i-1) for i in range(1,n))

if n > 2:
	slice_args = ', ' + ', '.join('size_t x%d' % (i+1) for i in range(2,n))
	slice_index = ',' + ','.join('x%d' % (i+1) for i in range(2,n))
	dump_loop = \
		'\n\t\t\t'.join(
			'for (size_t x{0}=0; x{0}<_dims[{0}-1]; x{0}++) {{'.format(i+1) \
				for i in range(2,n)) + \
		'\n\t\t\t\tout << "slice[-,-" << ' + \
		'<< '.join(" ',' << x%d" % (i+1) for i in range(2,n)) + \
		'<< "] =\\n";\n\t\t\t\t'
	dump_loop_end = '\n' + ''.join('\t\t\t}\n' * (n-2))
else:
	slice_args = ''
	slice_index = ''
	dump_loop = ''
	dump_loop_end = ''

template = Template("""
#include <iostream>

template <class T>
class ${name}
{
	public:
		${name}(${constructor_args}) {
			${set_dims}
			_subdims[0] = _dims[1];
			for (size_t i=2; i<${n}; i++) {
				_subdims[i-1] = _subdims[i-2] * _dims[i];
			}
			_size = ${size_calc};
			_data = new T[_size];
		}

		~${name}() {
			delete _data;
		}

		const T* data() const {
			return (const T*)_data;
		}

		T& operator()(${index_args}) {
			return _data[x1 + ${index_calc}];
		}

		const T& operator()(${index_args}) const {
			return _data[x1 + ${index_calc}];
		}

		const void dump_slice(std::ostream& out${slice_args}) const {
			for (size_t x2=0; x2<_dims[1]; x2++) {
				out << _data[${index_calc}];
				for (size_t x1=1; x1<_dims[0]; x1++) {
					out << ' '
					    << _data[x1 + ${index_calc}];
				}
				out << '\\n';
			}
		}

		const void dump(std::ostream& out) const {
			${dump_loop}dump_slice(out${slice_index});${dump_loop_end}
		}

	private:
		T* _data;
		size_t _size;
		size_t _dims[${n}];
		size_t _subdims[${n}-1];
};""")

with open(filename, 'w') as f:
	print >>f, __doc__
	print >>f, template.substitute(globals())

