# moab.py - Python interface to Moab scheduler utilities
#
# Copyright 2011, Brown University, Providence, RI. All Rights Reserved.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose other than its incorporation into a
# commercial product is hereby granted without fee, provided that the
# above copyright notice appear in all copies and that both that
# copyright notice and this permission notice appear in supporting
# documentation, and that the name of Brown University not be used in
# advertising or publicity pertaining to distribution of the software
# without specific, written prior permission.
#
# BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
# INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY
# PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR
# ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#
# Author: Mark Howison <mhowison@brown.edu>

"""Provides access to the Moab scheduler's job listings, through the XML output
of the showq utility."""

import re
import types
from collections import defaultdict
from subprocess import Popen
from subprocess import PIPE
from xml.dom import minidom
from torque import qstat

### Utility functions ###

def t2str(seconds):
	"""Convert time in seconds to a days/hours/minutes string '[Dd] HHh MM'"""
	s = seconds % 60
	m = (seconds - s)/60 % 60
	h = (seconds - m*60 - s)/3600 % 24
	d = (seconds - h*3600 - m*60 - s)/86400
	tokens = []
	if d > 0:
		tokens.append('%dd' % d)
	tokens.append('%2dh' % h)
	tokens.append('%02d' % m)
	return ' '.join(tokens)

### Data structures ###

states = ('R', 'Q', 'B', 'C', '?')
colors = {'R': 'green', 'Q': 'yellow', 'B': 'red', 'C': 'blue', '?': 'magenta'}

def get_state_abbv(fullname):
	state_abbv = {'active': 'R', 'eligible': 'Q', 'blocked': 'B', 'completed': 'C'}
	return state_abbv.get(fullname, '?')

def get_state_name(abbv):
	state_name = {'?': 'other', 'R': 'running', 'Q': 'queued', 'B': 'blocked', 'C': 'completed'}
	return state_name.get(abbv)

class Job(dict):

 	def __init__(self, attribute_list):
		dict.__init__(self)
		for key,val in attribute_list:
			self.__setitem__(key, val)
		try:
			remaining = int(self.get('ReqAWDuration')) - int(self.get('AWDuration'))
		except TypeError:
			remaining = 0
		self.__setitem__('remaining', remaining)
		try:
			priority = int(self.get('StartPriority'))
		except TypeError:
			priority = 0
		self.__setitem__('priority', priority)

	def get_walltime(self):
		try:
			return t2str(int(self.get('ReqAWDuration')))
		except TypeError:
			return ''

	def get_remaining(self):
		remaining = self['remaining']
		return t2str(remaining) if remaining > 0 else ''

	def get_priority(self):
		return '[%d]' % self['priority']

	def get_nodes(self):
		nodes = self.get('ReqNodes')
		# Handle jobs that have a single named node
		if not nodes:
			j = qstat(id=self.get('JobID'))[0]
			str = j.get('req_nodes')
			return str[:str.find(':')]
		else:
			return nodes


### Functions for accessing Moab utilities ###

def showq(user=None, group=None, moabclass=None, byclass=False):
	"""Returns a hierarchical data structure of jobs parsed from the showq XML output. The structure is a dict with state abbreviation as the key (see table below), storing dicts with the Moab class as key and the values as lists of Job objects. For example, accessing the second running job with "default" class would be accomplished with:

moab.showq()['R']['default'][1]

Alternatively, the ordering of states and classs can be reversed by passing the 'byclass' argument, so that the previous lookup becomes:

moab.showq(byclass=True)['default']['R'][1]

State Abbreviations
-------------------
'R' -> 'active'
'Q' -> 'eligible'
'B' -> 'blocked'
'C' -> 'completed'
'?' -> all others

These abbreviations are accessible in this order from the moab.states list.
"""
	# Turn list arguments into comma-separated '-w' string for showq.
	args = (('user',user), ('group', group), ('class', moabclass))
	wlist = ','.join(['='.join(arg) for arg in args if arg[1]])

	# Run showq with XML output.
	cmd = ['showq', '--xml']
	if len(wlist):
		cmd.append('-w')
		cmd.append(wlist)
	p = Popen(cmd, stdout=PIPE)

	# Parse XML output using minidom.
	doc = minidom.parseString(p.communicate()[0])

	# Build output dictionary.
	output = defaultdict(lambda: defaultdict(list))
	for node in doc.childNodes[0].getElementsByTagName('queue'):
		s = get_state_abbv(node.attributes.get('option').value)
		for child in node.childNodes:
			job = Job(child.attributes.items())
			q = str(job.get('Class'))
			if byclass:
				output[q][s].append(job)
			else:
				output[s][q].append(job)

	return output


def showstart(jobid):
	"""Returns an integer with the estimated start time in seconds for the given job. If the start time can't be determined, -1 is returned."""
	p = Popen(['showstart', jobid], stdout=PIPE)
	match = re.search('start in\s*?(\S+?) on', p.communicate()[0])
	if match:
		tokens = [int(t) for t in match.group(1).split(':')]
		if len(tokens) == 3:
			return int(tokens[2]+tokens[1]*60+tokens[0]*3600)
		elif len(tokens) == 4:
			return int(tokens[3]+tokens[2]*60+tokens[1]*3600+tokens[0]*86400)
	return -1

