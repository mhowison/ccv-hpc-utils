# torque.py - Python interface to Torque batch system utilities
#
# Copyright 2011, Brown University, Providence, RI. All Rights Reserved.
#
# Permission to use, copy, modify, and distribute this software and its
# documentation for any purpose other than its incorporation into a
# commercial product is hereby granted without fee, provided that the
# above copyright notice appear in all copies and that both that
# copyright notice and this permission notice appear in supporting
# documentation, and that the name of Brown University not be used in
# advertising or publicity pertaining to distribution of the software
# without specific, written prior permission.
#
# BROWN UNIVERSITY DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE,
# INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR ANY
# PARTICULAR PURPOSE.  IN NO EVENT SHALL BROWN UNIVERSITY BE LIABLE FOR
# ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
# WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
# ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
# OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#
# Author: Mark Howison <mhowison@brown.edu>

"""Provides access to the Torque batch system utilities, including pbsnodes and qstat."""

import locale
import types
import re
from subprocess import Popen
from subprocess import PIPE
from xml.dom import minidom

locale.setlocale(locale.LC_ALL, 'en_US')

### Utility functions ###

def kb2mb(kb):
	if kb:
		i = int(str(kb).split('kb')[0])
		return locale.format('%d', i/1024, grouping=True)
	else:
		return '-'

### Data structures ###

states = ('R', 'Q', 'C')
colors = {'R': 'green', 'Q': 'yellow', 'C': 'blue'}

class Job(dict):
	"""Encapsulates a <Job> entry in the qstat XML output as a dictionary."""
 	def __init__(self, xmlNode):
		dict.__init__(self)
		for child in xmlNode.childNodes:
			if child.nodeName == 'Resource_List':
				for subchild in child.childNodes:
					key = str('req_%s' % subchild.nodeName)
					self.__setitem__(key, str(subchild.childNodes[0].nodeValue))
			elif child.nodeName == 'resources_used':
				for subchild in child.childNodes:
					key = str('used_%s' % subchild.nodeName)
					self.__setitem__(key, str(subchild.childNodes[0].nodeValue))
			else:
				self.__setitem__(
						str(child.nodeName),
						str(child.childNodes[0].nodeValue))

	def get_id(self):
		id = self.get('Job_Id')
		return id[:id.find('.')] if id else None

	def get_user(self):
		owner = self.get('Job_Owner')
		return owner[:owner.find('@')] if owner else None

class Node(dict):
	"""Encapsulates a <Node> entry in the pbsnodes XML output as a dictionary."""
 	def __init__(self, xmlNode):
		dict.__init__(self)
		for child in xmlNode.childNodes:
			if child.nodeName == 'status':
				for entry in child.childNodes[0].nodeValue.split(','):
					fields = entry.strip().split('=')
					if len(fields) == 2:
						key = str('status_%s' % fields[0])
						self.__setitem__(key, str(fields[1]))
			else:
				self.__setitem__(
						str(child.nodeName),
						str(child.childNodes[0].nodeValue))

	def get_state(self):
		jobs = self.get('status_jobs')
		return jobs if jobs else '[free]'

	def get_load(self):
		load = self.get('status_loadave')
		return load if load else '-'

	def get_physmem(self):
		return kb2mb(self.get('status_physmem'))

	def get_availmem(self):
		return kb2mb(self.get('status_availmem'))

### Functions for accessing Torque utilities ###

def qstat(queue=None, id=None):
	# Run qstat with XML output.
	cmd = ['qstat', '-tx']
	for arg in (queue,id):
		if type(arg) in (types.TupleType, types.ListType):
			cmd.extend(arg)
		elif arg:
			cmd.append(arg)
	p = Popen(cmd, stdout=PIPE)

	# Parse XML output using minidom.
	doc = minidom.parseString(p.communicate()[0])

	# Build output list.
	xmlNodes = doc.childNodes[0].getElementsByTagName('Job')
	return [Job(node) for node in xmlNodes]

def pbsnodes(nodelist=None):
	# Run pbsnodes with XML output.
	cmd = ['pbsnodes', '-x']
	if type(nodelist) in (types.TupleType, types.ListType):
		cmd.extend(nodelist)
	elif nodelist:
		cmd.append(nodelist)
	p = Popen(cmd, stdout=PIPE)

	# Parse XML output using minidom.
	doc = minidom.parseString(p.communicate()[0])

	# Build output list.
	xmlNodes = doc.childNodes[0].getElementsByTagName('Node')
	return [Node(node) for node in xmlNodes]


def count_nodes(property):
	if property == 'dq':
		property = 'default'
	n = 0
	for node in pbsnodes():
		if node.get('properties').count(property):
			n += 1
	return n

def list_queues():
	p = Popen(['qstat', '-q'], stdout=PIPE)
	# Return the list of matches, excluding the first item, which is always the
	# header 'Queue'.
	return re.findall(r'\n(\w+) ', p.communicate()[0])[1:]

