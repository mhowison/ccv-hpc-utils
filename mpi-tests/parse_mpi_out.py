import ccv.plot as pd
import matplotlib.pyplot as plt
import re
import math
import os

#helper functions
def bitsToMB(bits):
  if bits < 1024:
    return str(bits) + ' b'
  elif bits < 1048576:
    return str(bits / 1024) + ' kB'
  else:
    return str(bits / 1048576) + ' MB'

#return only lines associated with the test string
def test_search(begin_str,end_str,a_lines):
  get_on = 0
  result = []
  for line in a_lines:
    if get_on==0:
      if re.search(begin_str,line):
        get_on=1
    else:
      if re.search(end_str, line):
        break
      if re.search('[Ee]rror',line) or re.search('segmentation',line): #stops on error or fault
        print "Warning: Possible errors or segmentation faults detected in the output file - graph may not be complete"
        break #stop search
      if re.search('^[0-9. ]+$', line):
        result.append(line)
  return result
#end helper functions

#OSU_Search:
#Inputs:
#test_type: "bandwidth" or "latency".  The type of test affects if the minimum (latency) or maximum (bandwidth) read in is "best."  Default: latency
#args: a list of openmpi module labels
#Returns:
#plotobject graphing results for that test, sorted by openmpi module.

def osu_search(str1,test_type,args): #args are a list of filenames (for each openmpi module)

  colors = ['blue','red','green','purple']
  markers = ['x','o','^','_']
  str2='OSU'

  p=pd.PlotData()
  p.setGraphType(plt.plot)
  color_id=0

  for pattern in args: #for each openmpi module (filename)

    y_best = {}
    new_ser = pd.Series([],[],pattern,color=colors[color_id],marker=markers[color_id])

    #fetch all files matching this pattern
    a_dir = os.listdir(os.getcwd())
    for s_file in a_dir:
      if re.search(pattern,s_file): #open this file for processing
        print s_file
        f=open(s_file,'r')
        a_lines = f.readlines()
        a_tmp = test_search(str1,str2,a_lines) 
        for line in a_tmp:
          m = re.search('^([0-9.]*)[ ]*([0-9.]*)\n$',line) #match
          if (m != None):
            x = m.group(1)
            y = m.group(2)
            if not(y_best.has_key(int(x))):
              y_best[int(x)] = float(y)
            else:
              if test_type == "bandwidth":
                if float(y) > y_best[int(x)]:
                  y_best[int(x)] = float(y) #set new maximum
              else: #latency
                if float(y) < y_best[int(x)]:
                  y_best[int(x)] = float(y) #set new min
    x_sorted = sorted(y_best.keys())
    for x in range(0,len(x_sorted)):
      new_ser.addPoint(x,math.log(float(y_best[x_sorted[x]])))
    p.addSeriesObj(new_ser)
    p.setTitle(str1)
    p.setXticks(range(0,len(x_sorted)))
    p.setXticklabels([bitsToMB(x) for x in x_sorted])
    color_id+=1
  return p

#Takes the name of an IMB test, and a list of openmpi module labels.
#Returns a plotobject graphing results for that test, sorted by openmpi module.
#Inputs:
#test_type: "bandwidth" or "latency".  Affects if we take the minimum or maximum read in.  Default: latency
#args: a list of openmpi module labels

def imb_search(str1,test_type,args):

  colors = ['blue','red','green','purple']
  markers = ['x','o','^','_']
  str2='Finalize'

  p=pd.PlotData()
  p.setGraphType(plt.plot)
  color_id=0

  for pattern in args: #for each pattern (module)

    y_best = {}
    new_ser = pd.Series([],[],pattern,color=colors[color_id],marker=markers[color_id])

    #fetch all files matching this pattern
    a_dir = os.listdir(os.getcwd())
    for s_file in a_dir:
      if re.search(pattern,s_file): #open this file for processing
        f=open(s_file,'r')
        a_lines = f.readlines()
        a_tmp = test_search(str1,str2,a_lines) 
        for line in a_tmp:
          m = re.search('^[ ]*([0-9.]+)[ ]+[0-9. ]+[ ]+([0-9.]+)[ ]*\n$',line) #match
          if (m != None):
            x = m.group(1)
            y = m.group(2)
            if not(y_best.has_key(int(x))):
              y_best[int(x)] = float(y)
            else:
              if test_type == "bandwidth":
                if float(y) > y_best[int(x)]:
                  y_best[int(x)] = float(y) #set new maximum
              else: #latency
                if float(y) < y_best[int(x)]:
                  y_best[int(x)] = float(y) #set new min
    x_sorted = sorted(y_best.keys())
    for x in range(0,len(x_sorted)):
      new_ser.addPoint(x,math.log(float(y_best[x_sorted[x]])))
    p.addSeriesObj(new_ser)
    p.setTitle(str1)
    p.setXticks(range(0,len(x_sorted)))
    p.setXticklabels([bitsToMB(x) for x in x_sorted])
    color_id+=1

    #mo = re.search("^imb-([-a-z0-9.]+).sh.e[0-9]+$",arg) #get label for this module
    #label = pattern

  return p
