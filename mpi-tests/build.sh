#!/bin/bash

set -o errexit

#MPI_LIST="openmpi/1.6 mvapich2/1.8"
#MPI_LIST="mvapich2/1.9a"
MPI_LIST="openmpi/1.6.2"

IMB=${IMB-1}
OSU=${OSU-1}
START_DIR=$PWD

if [ $IMB -gt 0 ]
then
	echo "Building IMB..."
	tar xf IMB_3.2.3.tgz
	cd imb_3.2.3/src
	sed -i -e '/CC.*= / c CC = mpicc -O2 -g ' make_ict
	# set max message size to 1GB
	sed -i -e '/MAXMSGLOG/ c #define MAXMSGLOG 30' IMB_settings.h

	for mpi in $MPI_LIST
	do
		echo $mpi
		module unload $MPI_LIST
		module load $mpi
		gmake clean
		gmake MPI1
		cp IMB-MPI1 ../../imb-${mpi/\//-}
	done
fi

cd $START_DIR

if [ $OSU -gt 0 ]
then
	echo "Building OSU..."
	tar xf osu-micro-benchmarks-3.6.tar.gz
	cd osu-micro-benchmarks-3.6

	for mpi in $MPI_LIST
	do
		echo $mpi
		module unload $MPI_LIST
		module load $mpi
		./configure CC=mpicc
		make clean
		make
		install_dir="../osu-${mpi/\//-}"
		mkdir -p $install_dir
		find . -name 'osu_*' -perm 755 -exec cp {} $install_dir/ \;
	done
fi

