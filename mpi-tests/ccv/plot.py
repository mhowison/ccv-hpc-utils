#!/usr/bin/python

#File:           plot.py
#Version:        0.1
#Purpose:        Wrapper for matplotlib library
#Author:         Aaron Shen <aaron_shen@brown.edu>

import matplotlib.pyplot as plt
import matplotlib.colors as col
import math
import sys

def isNumber(x):
  try:
    x + 1
    return True
  except TypeError:
    return False

# custom sort function, in reverse order of height
def yHeightSort(i,j):
    if j.get_height() > i.get_height(): return 1
    else: return -1

#
#Plotdata: Class for storing a graph object and interfacing with matplotlib.  Each PlotData object contains 
#information for a single matplotlib.Axes object.
class Series:
  def __init__(self,a_xdata,a_ydata,label,**kwargs):
    if len(a_xdata) != len(a_ydata): #check for data integrity
      print "Series x and y data cannot be different sizes"
      sys.exit()
    self.xdata = a_xdata #an array of data for this series.
    self.ydata = a_ydata #an array of data for this series.
    self.label = label
    self.keywords = kwargs #keywords for color, linestyle, etc

  def getNumPoints(self): #get the number of points in this series.
    return len(a_xdata)

  def addPoint(self,x,y): #add a new x,y point to this series
    if not (isNumber(x) or isNumber(y)):
      print "Series data must be numeric"
    self.xdata.append(x)
    self.ydata.append(y)
      
class PlotData:
  def __init__(self):
    self.a_series = [] #list of series objects in this plot
    self.a_xticks = None #a single array defining the xticks.  If empty, plot makes best guess
    self.a_yticks = None #a single array defining the yticks.  If empty, plot makes best guess

    #Optional features of graph
    self.a_xticklabels=None #Labels for data on x-axis.
    self.a_yticklabels=None #Labels for data on y-axis.
    self.xlabel=None #Label of x-axis
    self.ylabel=None #Label of y-axis
    self.graph_fxn = plt.plot
    self.title=None #Graph title
    self.legend_args = None
    self.x_grid_args = None
    self.y_grid_args = None

  #adding data to graph
  def addSeriesObj(self,new_series): #add a preexisting series object to the graph
    self.a_series.append(new_series)

  def addData(self,new_series_x,new_series_y,name,**kwargs): #add a series to the graph with a specific name and color
    self.a_series.append(Series(new_series_x,new_series_y,name,**kwargs))

  #changing appearance of graph
  def setXticks(self,x):
    self.a_xticks=x

  def setYticks(self,y):
    self.a_yticks=y

  def setXticklabels(self,xticklabels): #Set individual xtick labels (must be a list)
    self.a_xticklabels=xticklabels

  def setYticklabels(self,yticklabels): #Set individual ytick labels (must be a list)
    self.a_yticklabels=yticklabels

  def setXlabel(self,xlabel): #Set label of y-axis
    self.xlabel=xlabel

  def setYlabel(self,ylabel): #Set label of y-axis
    self.ylabel=ylabel

  def setTitle(self,title): #title of the graph
    self.title=title

  def setGraphType(self,graph_fxn): #what kind of graph (bar,line,etc) this is
    self.graph_fxn = graph_fxn

  def showLegend(self, **kwargs):
    self.legend_args = kwargs

  def showXGrid(self,**kwargs):
    self.x_grid_args = kwargs

  def showYGrid(self,**kwargs):
    self.y_grid_args = kwargs

  def showPlot(self): #plots the data using matplotlib.  TO BE IMPLEMENTED
    fig = plt.figure()
    ax=fig.add_subplot(1,1,1)
    self.plotDataOnAxes(ax)

    #make patches "transparent"
    all_patches = ax.patches
    patch_at_x = {}
    for patch in all_patches:
        if patch.get_x() not in patch_at_x: patch_at_x[patch.get_x()] = []
        patch_at_x[patch.get_x()].append(patch)

    # loop through sort assign z-order based on sort
    for x_pos, patches in patch_at_x.iteritems():
        if len(patches) == 1: continue
        patches.sort(cmp=yHeightSort)
        [patch.set_zorder(patches.index(patch)) for patch in patches]

    plt.show()

  #input: a new (current) axes object.  Makes the axes object represent the plotdata.
  def plotDataOnAxes(self,ax):
    for ser in range(0,len(self.a_series)): #plot the data
      self.graph_fxn(self.a_series[ser].xdata,self.a_series[ser].ydata,label=self.a_series[ser].label,**self.a_series[ser].keywords)

    if (self.a_xticks != None): #scan for plotdata settings
      ax.set_xticks(self.a_xticks)
    if (self.a_yticks != None):
      ax.set_yticks(self.a_yticks)
    if (self.xlabel != None): 
      ax.set_xlabel(self.xlabel)
    if (self.ylabel != None):
      ax.set_ylabel(self.ylabel)
    if (self.title != None):
      ax.set_title(self.title)
    if (self.a_xticklabels != None):
      ax.set_xticklabels(self.a_xticklabels, rotation='vertical',size='x-small',stretch='extra-condensed')
    if (self.a_yticklabels != None):
      ax.set_yticklabels(self.a_yticklabels,size='x-small',stretch='extra-condensed')
    if (self.legend_args !=None):
      ax.legend(**self.legend_args)
    if (self.x_grid_args != None):
      ax.get_xaxis().grid(**self.x_grid_args)
    if (self.y_grid_args != None):
      ax.get_yaxis().grid(**self.y_grid_args)  

#create a multi-windowed graph from a list of PlotData instances (each PlotData instance will
#be a subplot on the graph)
#acceptable kwargs:
#linestyles = a list of linestyles
#colors = a list of colors

class MultiPlot: #specify colors, etc at graph time
  def __init__(self,width,height,per_row,plotdatas): #width and height of each individual plot.  Per_row = # of plots per row
    col_size = math.ceil(len(plotdatas)/float(per_row))
    row_size = per_row
    self.fig = plt.figure(figsize=(row_size*width,col_size*height))
    self.fig.subplots_adjust(hspace=0.4)
    self.fig.subplots_adjust(wspace=0.3)

    for field in range(0,len(plotdatas)): #set params of each plot
      ax=self.fig.add_subplot(col_size,row_size,field+1)
      plotdatas[field].plotDataOnAxes(ax)

      #make patches "transparent"
      all_patches = ax.patches
      patch_at_x = {}
      for patch in all_patches:
          if patch.get_x() not in patch_at_x: patch_at_x[patch.get_x()] = []
          patch_at_x[patch.get_x()].append(patch)

      # loop through sort assign z-order based on sort
      for x_pos, patches in patch_at_x.iteritems():
          if len(patches) == 1: continue
          patches.sort(cmp=yHeightSort)
          [patch.set_zorder(patches.index(patch)) for patch in patches]

  def showGraph(self):
    plt.show()

  def saveGraph(self,name):
    plt.savefig(name)

