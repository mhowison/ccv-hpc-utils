import ccv.plot as pd
import matplotlib.pyplot as plt
import re
import math
import os
import parse_mpi_out as pmo


IMB_LENGTHS1 = 'imb_lengths.txt'
IMB_LENGTHS2 = 'imb_lengths_long.txt'

#file patterns matching the desired modules
fn1 = 'osu-mvapich2.out'
fn2 = 'osu-openmpi.out'
fn3 = 'imb-mvapich2.out'
fn4 = 'imb-openmpi.out'

test_arr = []
tmp1=[fn1,fn2]
po =pmo.osu_search('Bi-Directional Bandwidth','bandwidth',tmp1)
po.showLegend(loc='lower right',prop={'size':8})
po.setYlabel('log MB/s')
po.showYGrid()
test_arr.append(po)
po = pmo.osu_search('Bandwidth','bandwidth',tmp1)
po.setYlabel('log MB/s')
po.showYGrid()
test_arr.append(po)

#graph latency plots
for test in ['Accumulate Latency','Allgather Latency','Allgatherv Latency','Allreduce Latency','All-to-All', \
'All-to-Allv','Broadcast Latency','Gather Latency','Gatherv Latency']:
  po=pmo.osu_search(test,'latency',tmp1)
  po.setYlabel('log usec')
  po.showYGrid()
  test_arr.append(po)

mp = pd.MultiPlot(5,3,4,test_arr)
#mp.showGraph()
mp.saveGraph('osu.pdf')

test_arr2 = []
tmp2=[fn3,fn4]
#graph mbytes/sec graphs
po = pmo.imb_search('Sendrecv','bandwidth',tmp2)
po.showLegend(loc='lower right',prop={'size':8})
po.setYlabel('log Mbytes/sec')
po.showYGrid()
test_arr2.append(po)
po= pmo.imb_search('Exchange','bandwidth',tmp2)
po.setYlabel('log Mbytes/sec')
po.showYGrid()
test_arr2.append(po)

#graph all others (usecs)
for test in ['Reduce','Reduce_scatter','Allgather','Allgatherv','Gather','Gatherv','Scatter','Scatterv','Alltoall', \
'Alltoallv','Bcast']:
  po= pmo.imb_search(test,'latency',tmp2)
  po.setYlabel('log usec')
  po.showYGrid()
  test_arr2.append(po)

mp2 = pd.MultiPlot(3,3,4,test_arr2)
#mp2.showGraph()
mp2.saveGraph('imb.pdf')




#return a plotdata object from the following files:
#def osu_search(str1,args): #args are a list of filenames (for each openmpi module)

#  colors = ['blue','red','green','purple']
#  markers = ['x','o','^','_']
#  str2='OSU'

#  p=pd.PlotData()
#  p.setGraphType(plt.plot)
#  color_id=0

#  for arg in args: #for each openmpi module (filename)

#    mo = re.search("^osu-([-a-z0-9.]+).sh.e[0-9]+$",arg)#get label for this module
#    label = mo.group(1)

#    f = open(arg,'r')
#    a_lines = f.readlines()

#    a_tmp = test_search(str1,str2,a_lines) 
#    a_x = []
#    a_xl = []
#    a_y = []
#    num = 0
#    for line in a_tmp:
#      m = re.search('^([0-9.]*)[ ]*([0-9.]*)\n$',line) #match
#      if (m != None):
#        x = m.group(1)
#        y = m.group(2)
#        a_x.append(num)
#        a_xl.append(bitsToMB(int(x)))
#        a_y.append(math.log(float(y)))
#        num+=1
#    p.addData(a_x,a_y,label,color=colors[color_id],marker=markers[color_id])
#    p.setTitle(str1)
#    p.setXticks(a_x)
#    p.setXticklabels(a_xl)
#    p.showYGrid()
#    color_id+=1
#  return p

