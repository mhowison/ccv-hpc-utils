#!/bin/bash

set -o errexit

#MPI_LIST="openmpi mvapich2"
#MPI_LIST="mvapich2/1.9a"
MPI_LIST="mvapich2/1.9a openmpi/1.6.2"

QSUB=${QSUB-"sbatch"}

PBS_HEADER="
#!/bin/bash
\n#SBATCH -C e5-2600
\n#SBATCH --time=10:00
"

IMB_TESTS="
Sendrecv
Exchange
Allreduce
Reduce
Reduce_scatter
Allgather
Allgatherv 
Gather
Gatherv
Scatter
Scatterv
Alltoall
Alltoallv
Bcast
Barrier"

for mpi in $MPI_LIST
do
	name="imb-${mpi/\//-}"
	mkdir -p $name
	cp imb_lengths.txt $name/
	pbs="$name/$name.sh"
	echo -e $PBS_HEADER >$pbs
        echo "#SBATCH --exclusive" >>$pbs
	echo "#SBATCH --nodes=4" >>$pbs
        echo "#SBATCH --ntasks-per-node=8" >>$pbs
	echo "cd $PWD/$name" >>$pbs
	echo "module unload $MPI_LIST" >>$pbs
	echo "module load $mpi" >>$pbs
	for t in $IMB_TESTS
        do
		echo "mpiexec -n 32 $PWD/../build/$name -msglen imb_lengths.txt -npmin 32 -mem 2 -iter 1000,128 $t" >>$pbs
        done
  echo "$QSUB $pbs"
	$QSUB $pbs
	cp imb_lengths_long.txt $name/
	pbs="$name/$name-long.sh"
	echo -e $PBS_HEADER >$pbs
        echo "#SBATCH --exclusive" >>$pbs
	echo "#SBATCH --nodes=4" >>$pbs
        echo "#SBATCH --ntasks-per-node=2" >>$pbs
	echo "cd $PWD/$name" >>$pbs
	echo "module unload $MPI_LIST" >>$pbs
	echo "module load $mpi" >>$pbs
	for t in $IMB_TESTS
	do
		echo "mpiexec -n 8 $PWD/../build/$name -msglen imb_lengths_long.txt -npmin 8 -mem 10 -iter 1000,512 $t" >>$pbs
	done
  echo "$QSUB $pbs"
	$QSUB $pbs

done

OSU_TESTS="
osu_acc_latency
osu_allgather
osu_allgatherv
osu_allreduce
osu_alltoall
osu_alltoallv
osu_barrier
osu_bcast
osu_bibw
osu_bw
osu_gather
osu_gatherv
osu_get_bw
osu_get_latency
osu_latency
osu_mbw_mr
osu_passive_acc_latency
osu_passive_get_bw
osu_passive_get_latency
osu_passive_put_bw
osu_passive_put_latency
osu_put_bibw
osu_put_bw
osu_put_latency
osu_reduce
osu_reduce_scatter
osu_scatter
osu_scatterv
"

for mpi in $MPI_LIST
do
	name="osu-${mpi/\//-}"
	mkdir -p $name
	pbs="$name/$name.sh"
	echo -e $PBS_HEADER >$pbs
        echo "#SBATCH --exclusive" >>$pbs
        echo "#SBATCH --nodes=2" >>$pbs
        echo "#SBATCH --ntasks-per-node=1" >>$pbs
	echo "cd $PWD/$name" >>$pbs
	echo "module unload $MPI_LIST" >>$pbs
	echo "module load $mpi" >>$pbs
	for t in $OSU_TESTS
	do
		echo "mpiexec -n 2 $PWD/../build/$name/$t" >>$pbs
	done
  echo "$QSUB $pbs"
	$QSUB $pbs
done

