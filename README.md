# ccv-hpc-utils

This is a collection of scripts and utilities developed at the Center for
Computation and Visualization, Brown University, for administering the Oscar
compute cluster.

## License

Free for non-commercial use. See LICENSE for full details.

## Authors

Mark Howison  
Aaron Shen
Sam Fulcomer

